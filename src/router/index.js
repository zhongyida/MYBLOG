import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const previewUserInfo = (resolve) => {
  import('../components/timestamp/timestamp').then((module) => {

    resolve(module);
  });
};
let routes = [
  {
    path: '/',
    name: 'index'
  }, {
    path: "/timestamp",
    name: "timestamp",
    component: previewUserInfo
  }
]

const router = new Router({
  mode: 'history',
  routes
})
export default router
